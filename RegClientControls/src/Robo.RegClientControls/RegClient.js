﻿Type.registerNamespace("Robo");
Robo._PageControls = function() {
	Robo._PageControls.initializeBase(this);
}
Robo._PageControls.prototype = {
	add_ready: function(handler) {
		this.get_events().addHandler("ready", handler);
	},
	remove_ready: function(handler) {
		this.get_events().removeHandler("ready", handler);
	},
	raiseReady: function() {
		var h = this.get_events().getHandler('ready');
		if (h) h(this);
	},
	registerObject: function(object){
		for (x in object){
			eval('this.' + object[x].c + ' = $get(object[x].s)');
		}
		this.raiseReady();
	}
}
Robo._PageControls.registerClass('Robo._PageControls', Sys.Component);
var PageControls = new Robo._PageControls();
if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();