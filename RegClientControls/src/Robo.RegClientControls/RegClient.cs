﻿using System;
using System.ComponentModel;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("Robo.RegClientControls.RegClient.js", "text/javascript")]

namespace Robo.RegClientControls {

	public enum ControlsToReg { All, Marked }

	[Designer(typeof(RegClientDesigner))]
	public class RegClient : Control {
		
		#region "Fields"

		private ControlsToReg _clientControlsRegister;

		#endregion

		#region "Constructors"

		public RegClient() {
			Init += RegClient_Init;
			PreRender += RegClient_PreRender;

			ClientControls = ControlsToReg.Marked;
		}

		#endregion

		#region "Methods"

		void RegClient_Init(object sender, EventArgs e) {
			string csURL = "Robo.RegClientControls.RegClient.js";
			ScriptManager.GetCurrent(Page).Scripts.Insert(0, new ScriptReference(Page.ClientScript.GetWebResourceUrl(GetType(), csURL)));
		}


		void RegClient_PreRender(object sender, EventArgs e) {
			if (!DesignMode) {
				StringBuilder builder = new StringBuilder();
				builder.Append("PageControls.registerObject([");
				
				foreach (Control control in Page.Controls) {
					LoopControls(builder, control);
				}
				
				string objectArray = builder.ToString().TrimEnd(',') + "]);\n";
				Page.ClientScript.RegisterStartupScript(GetType(), "RegClient", objectArray, true);
			}
		}

		/// <summary>
		/// Recursive loop through all controls and register the ones that need it.
		/// </summary>
		/// <param name="builder">Output to append to.</param>
		/// <param name="control">Control to check.</param>
		void LoopControls(StringBuilder builder, Control control) {

			if (typeof(WebControl).IsAssignableFrom(control.GetType())) {
				WebControl webControl = (WebControl) control;

				if (DoRegControl(webControl)) {
					builder.Append("\n{'c':'" + GetFriendlyID(webControl) + "','s':'" + webControl.ClientID + "'},");
				}
			}

			if (control.HasControls()) {
				foreach (Control child in control.Controls) {
					LoopControls(builder, child);
				}
			}
		}

		/// <summary>
		/// Decide if this control should be registered.
		/// </summary>
		/// <param name="webControl">Control to check.</param>
		/// <returns></returns>
		private bool DoRegControl(WebControl webControl) {
			if (string.IsNullOrEmpty(webControl.ID)) {
				return false;
			}
			foreach (string key in webControl.Attributes.Keys) {
				if (key.ToLower() == "regclient") {
					return Convert.ToBoolean(webControl.Attributes[key]);
				}
				if (key.ToLower() == "clientcontrolid") {
					if (!string.IsNullOrEmpty(webControl.Attributes[key])) {
						return true;
					}
				}
			}
			switch (ClientControls) {
				case ControlsToReg.All:
					return true;
				case ControlsToReg.Marked:
					return false;
				default:
					return true;
			}
		}


		protected string GetFriendlyID(WebControl webControl) {
			if (!string.IsNullOrEmpty(webControl.Attributes["clientcontrolid"])) {
				return webControl.Attributes["clientcontrolid"];
			}
			return webControl.ID;
		}

		protected override void LoadControlState(object savedState) {
			object[] stateTmp = (object[])savedState;
			base.LoadControlState(stateTmp[0]);
			ClientControls = (ControlsToReg)stateTmp[1];
		}


		protected override object SaveControlState() {
			object[] state = new object[2];
			state[0] = base.SaveControlState();
			state[1] = ClientControls;
			return state;
		}

		#endregion

		#region "Propertes"

		/// <summary>
		/// What controls should be registered.
		/// </summary>
		[DefaultValue(ControlsToReg.Marked)]
		public ControlsToReg ClientControls {
			get { return _clientControlsRegister; }
			set { _clientControlsRegister = value; }
		}

		#endregion
	}
}
