﻿using System.ComponentModel.Design;
using System.Web.UI.Design;

namespace Robo.RegClientControls {
	public class RegClientDesigner : ControlDesigner {
		private DesignerActionListCollection _actions;

		public override DesignerActionListCollection ActionLists {
			get {
				if (_actions == null) {
					_actions = new DesignerActionListCollection();

					_actions.Add(new RegClientActionList((RegClient) Component));
				}
				return _actions;
			}
		}

		protected override string GetEmptyDesignTimeHtml() {
			return CreatePlaceHolderDesignTimeHtml();
		}
	}
}
