﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Text;

namespace Robo.RegClientControls {
	public class RegClientActionList:DesignerActionList {
		private RegClient _linkedControl;
		public RegClientActionList(RegClient ctrl) : base(ctrl) {
			_linkedControl = ctrl;
		}


		public override DesignerActionItemCollection GetSortedActionItems() {
			DesignerActionItemCollection items = new DesignerActionItemCollection();

			items.Add(new DesignerActionHeaderItem("Settings"));
			items.Add(new DesignerActionHeaderItem("Usage"));
			items.Add(new DesignerActionHeaderItem("Client Usage"));

			items.Add(new DesignerActionPropertyItem("ClientControls", "ClientControls", "Settings", "What controls should be registered."));

			StringBuilder sb = new StringBuilder();
			sb.AppendLine("Place this control on a page after a ScriptManager.");
			sb.AppendLine();
			sb.AppendLine("Valid attributes for WebControls are:");
			sb.AppendLine("RegClient=\"{Boolean}\"");
			sb.AppendLine("ClientControlID=\"{String}\"");
			sb.AppendLine();
			sb.AppendLine("If ClientControls=\"All\": ");
			sb.AppendLine("All WebControls will be registered to the client unless");
			sb.AppendLine("they are marked with RegClient=\"false\"");
			sb.AppendLine();
			sb.AppendLine("If ClientControls=\"Marked\": ");
			sb.AppendLine("The only WebControls registered to the client will");
			sb.AppendLine("be the ones marked with RegClient=\"true\"");
			sb.AppendLine("or the ones marked with ClientControlID=\"{yourClientName}\"");
			sb.AppendLine();
			sb.AppendLine("If you specify \"ClientControlID\" on a WebControl, ");
			sb.AppendLine("\"RegClient\" is not needed.");

			items.Add(new DesignerActionTextItem(sb.ToString(), "Usage"));

			StringBuilder sb2 = new StringBuilder();
			sb2.AppendLine("All registered controls are available from the \"PageControls\" object.");
			sb2.AppendLine("Handle the \"ready\" event of the \"PageControls\" object.");
			sb2.AppendLine("(The \"ready\" event gets fired when all controls have been registered)");
			sb2.AppendLine();
			sb2.AppendLine("<script language=\"javascript\" type=\"text/javascript\">");
			sb2.AppendLine("    PageControls.add_ready(function(){");
			sb2.AppendLine("        var textValue = PageControls.TextBox1.value;");
			sb2.AppendLine("    });");
			sb2.AppendLine("</script>");

			items.Add(new DesignerActionTextItem(sb2.ToString(), "Client Usage"));

			return items;

		}

		PropertyDescriptor GetPropertyByName(string propName) {
			PropertyDescriptor prop;
			prop = TypeDescriptor.GetProperties(_linkedControl)[propName];

			if (prop == null) {
				throw new ArgumentException("Matching property not found", propName);
			}
			else {
				return prop;
			}
		}

		public ControlsToReg ClientControls {
			get { return _linkedControl.ClientControls; }
			set { GetPropertyByName("ClientControls").SetValue(_linkedControl, value); }
		}

	}

}
