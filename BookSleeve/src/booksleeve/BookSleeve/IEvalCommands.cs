﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookSleeve
{
    public interface IEvalCommands
    {
        Task<long> EvalLong(int db, string body, int numOfArgs, bool queueJump = false, params string[] args);
        Task<string> EvalString(int db, string body, int numOfArgs, bool queueJump = false, params string[] args);
        Task<string[]> EvalMultiString(int db, string body, int numOfArgs, bool queueJump = false, params string[] args);
        Task<long> EvalShaLong(int db, string body, int numOfArgs, bool queueJump = false, params string[] args);
        Task<string> EvalShaString(int db, string body, int numOfArgs, bool queueJump = false, params string[] args);
        Task<string[]> EvalShaMultiString(int db, string body, int numOfArgs, bool queueJump = false, params string[] args);


    }

    partial class RedisConnection : IEvalCommands
    {
        //added by markv, 20111226
        Task<long> IEvalCommands.EvalLong(int db, string body, int numOfArgs, bool queueJump, params string[] args)
        {
            var allArgs = new string[args.Length + 2];
            args.CopyTo(allArgs, 2);
            allArgs[0] = body;
            allArgs[1] = numOfArgs.ToString();
            return ExecuteInt64(RedisMessage.Create(db, RedisLiteral.EVAL, allArgs), queueJump);
        }

        Task<string> IEvalCommands.EvalString(int db, string body, int numOfArgs, bool queueJump, params string[] args)
        {
            var allArgs = new string[args.Length + 2];
            args.CopyTo(allArgs, 2);
            allArgs[0] = body;
            allArgs[1] = numOfArgs.ToString();
            return ExecuteString(RedisMessage.Create(db, RedisLiteral.EVAL, allArgs), queueJump);
        }


        Task<string[]> IEvalCommands.EvalMultiString(int db, string body, int numOfArgs, bool queueJump, params string[] args)
        {
            var allArgs = new string[args.Length + 2];
            args.CopyTo(allArgs, 2);
            allArgs[0] = body;
            allArgs[1] = numOfArgs.ToString();
            return ExecuteMultiString(RedisMessage.Create(db, RedisLiteral.EVAL, allArgs), queueJump);
        }


        Task<long> IEvalCommands.EvalShaLong(int db, string body, int numOfArgs, bool queueJump, params string[] args)
        {
            var allArgs = new string[args.Length + 2];
            args.CopyTo(allArgs, 2);
            allArgs[0] = body;
            allArgs[1] = numOfArgs.ToString();
            return ExecuteInt64(RedisMessage.Create(db, RedisLiteral.EVALSHA, allArgs), queueJump);
        }

        Task<string> IEvalCommands.EvalShaString(int db, string body, int numOfArgs, bool queueJump, params string[] args)
        {
            var allArgs = new string[args.Length + 2];
            args.CopyTo(allArgs, 2);
            allArgs[0] = body;
            allArgs[1] = numOfArgs.ToString();
            return ExecuteString(RedisMessage.Create(db, RedisLiteral.EVALSHA, allArgs), queueJump);
        }


        Task<string[]> IEvalCommands.EvalShaMultiString(int db, string body, int numOfArgs, bool queueJump, params string[] args)
        {
            var allArgs = new string[args.Length + 2];
            args.CopyTo(allArgs, 2);
            allArgs[0] = body;
            allArgs[1] = numOfArgs.ToString();
            return ExecuteMultiString(RedisMessage.Create(db, RedisLiteral.EVALSHA, allArgs), queueJump);
        }


    }
}
