﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FizzWare.NBuilder.Tests.TestClasses;
using NUnit.Framework;

namespace FizzWare.NBuilder.Tests.Integration
{
    class ListBuilderTests_WithAClassThatIsBuiltFromANonParameterlessFactory
    {
        private MyNonParameterlessFactory _factory;

        [SetUp]
        public void SetUp()
        {
            _factory = new MyNonParameterlessFactory();
        }

        [Test]
        public void should_be_able_to_use_WithFactory()
        {
            int expectedInt = 1001;

            float expectedFloat = 2002.002f;

            string expectedString = "This class instantiated by a non-parameterless factory";

            decimal expectedDecimal = new decimal(3003.03);

            var list =
                Builder<MyClassWithFactory>
                    .CreateListOfSize(10)
                    .All()
                        .WithFactory(() => _factory.Create(expectedInt, expectedFloat, expectedString, expectedDecimal))
                    .Build();

            Assert.That(list.Count, Is.EqualTo(10));
            
            Assert.That(list.Select(x => x.Int), Is.All.EqualTo(expectedInt));

            Assert.That(list.Select(x => x.Float), Is.All.EqualTo(expectedFloat));

            Assert.That(list.Select(x => x.String), Is.All.EqualTo(expectedString));

            Assert.That(list.Select(x => x.Decimal), Is.All.EqualTo(expectedDecimal));
        }

    }
}
