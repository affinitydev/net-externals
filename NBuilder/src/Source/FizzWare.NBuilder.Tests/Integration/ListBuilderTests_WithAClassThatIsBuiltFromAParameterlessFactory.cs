﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FizzWare.NBuilder.Tests.TestClasses;
using NUnit.Framework;

namespace FizzWare.NBuilder.Tests.Integration
{
    class ListBuilderTests_WithAClassThatIsBuiltFromAParameterlessFactory
    {
        private MyParameterlessFactory _factory;

        [SetUp]
        public void SetUp()
        {
            _factory = new MyParameterlessFactory();
        }

        [Test]
        public void should_be_able_to_use_WithFactory()
        {
            var list =
                Builder<MyClassWithFactory>
                    .CreateListOfSize(10)
                    .All()
                        .WithFactory(() => _factory.Create())
                    .Build();

            Assert.That(list.Count, Is.EqualTo(10));

            Assert.That(list.Select(x => x.Int), Is.All.EqualTo(MyParameterlessFactory.ExpectedInt));

            Assert.That(list.Select(x => x.Float), Is.All.EqualTo(MyParameterlessFactory.ExpectedFloat));

            Assert.That(list.Select(x => x.String), Is.All.EqualTo(MyParameterlessFactory.ExpectedString));

            Assert.That(list.Select(x => x.Decimal), Is.All.EqualTo(MyParameterlessFactory.ExpectedDecimal));
        }
    }
}
