﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FizzWare.NBuilder.Tests.TestClasses
{
    class MyClassWithFactory : IMyClassWithFactory
    {
        public int Int { get; set; }
        public float Float { get; set; }
        public string String { get; set; }
        public decimal Decimal { get; set; }

        public MyClassWithFactory(int i, float f, string s, decimal d)
        {
            Int = i;
            Float = f;
            String = s;
            Decimal = d;
        }
    }
}
