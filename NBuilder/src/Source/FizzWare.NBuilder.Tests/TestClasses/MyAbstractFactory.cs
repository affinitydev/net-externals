﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FizzWare.NBuilder.Tests.TestClasses
{
    class MyAbstractFactory
    {
        public static int ExpectedInt = 1001;

        public static float ExpectedFloat = 2002.002f;

        public static string ExpectedString = "This class instantiated by a parameterless factory";

        public static decimal ExpectedDecimal = new decimal(3003.03);

        public IMyClassWithFactory Create()
        {
            return new MyClassWithFactory(ExpectedInt, ExpectedFloat, ExpectedString, ExpectedDecimal);
        }
    }
}
