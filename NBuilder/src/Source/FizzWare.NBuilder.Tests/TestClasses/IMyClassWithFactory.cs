﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FizzWare.NBuilder.Tests.TestClasses
{
    interface IMyClassWithFactory
    {
        int Int { get; set; }
        float Float { get; set; }
        string String { get; set; }
        decimal Decimal { get; set; }
    }
}
