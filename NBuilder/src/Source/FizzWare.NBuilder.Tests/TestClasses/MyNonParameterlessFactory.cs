﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FizzWare.NBuilder.Tests.TestClasses
{
    class MyNonParameterlessFactory
    {
        public MyClassWithFactory Create(int i, float f, string s, decimal d)
        {
            return new MyClassWithFactory(i, f, s, d);
        }
    }
}
